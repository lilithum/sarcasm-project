from config import *
from utils import *
# import sarcasm_classifier as classifier
import numpy as np
import tensorflow as tf
import pickle
from tensorflow.keras import layers , activations , models , preprocessing
from keras.layers import Embedding, Dense, LSTM
from keras.models import Model
import pandas as pd
from keras import utils
import re
import os
import pickle

def readGloveFile(gloveFile):
    with open(gloveFile, 'r', encoding='utf8') as f:
        wordToGlove = {}  # map from a token (word) to a Glove embedding vector
        wordToIndex = {}  # map from a token to an index
        indexToWord = {}  # map from an index to a token 

        for line in f:
            record = line.strip().split()
            token = record[0] # take the token (word) from the text line
            wordToGlove[token] = np.array(record[1:], dtype=np.float64) # associate the Glove embedding vector to a that token (word)

        tokens = sorted(wordToGlove.keys())
        for idx, tok in enumerate(tokens):
            kerasIdx = idx + 1  # 0 is reserved for masking in Keras (see above)
            wordToIndex[tok] = kerasIdx # associate an index to a token (word)
            indexToWord[kerasIdx] = tok # associate a word to a token (word). Note: inverse of dictionary above

    return wordToIndex, indexToWord, wordToGlove

# Create Pretrained Keras Embedding Layer
def createPretrainedEmbeddingLayer(wordToGlove, wordToIndex, isTrainable):
    vocabLen = len(wordToIndex) + 1  # adding 1 to account for masking
    print(vocabLen, 'vocablen')
    embDim = next(iter(wordToGlove.values())).shape[0]  # works with any glove dimensions (e.g. 50)

    embeddingMatrix = np.zeros((vocabLen, embDim))  # initialize with zeros
    for word, index in wordToIndex.items():
        embeddingMatrix[index, :] = wordToGlove[word] # create embedding: word index to Glove word embedding

    embeddingLayer = tf.keras.layers.Embedding(vocabLen, embDim, weights=[embeddingMatrix], trainable=isTrainable)
    return embeddingLayer


def create_model():
	#prepare embeddings
	wordToIndex, indexToWord, wordToGlove = readGloveFile(r"glove.6B." + str(GLOVE_DIM)+ "d.txt")
	pretrainedEmbeddingLayer = createPretrainedEmbeddingLayer(wordToGlove, wordToIndex, isTrainable=False)

	#Encoder part
	encoder_inputs = tf.keras.layers.Input(shape=(None,))   # Model() needs us to specify where inputs will enter the graph
	encoder_embedding = pretrainedEmbeddingLayer(encoder_inputs)
	encoder_outputs =  tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(GLOVE_DIM, return_state=True))
	encoder_outputs_, forward_h, forward_c, backward_h, backward_c = encoder_outputs(encoder_embedding)
	state_h = tf.keras.layers.Concatenate()([forward_h, backward_h])
	state_c = tf.keras.layers.Concatenate()([forward_c, backward_c])
	encoder_states = [state_h, state_c]

	#Decoder part
	decoder_inputs = tf.keras.layers.Input(shape=(None,))
	decoder_embedding = pretrainedEmbeddingLayer(decoder_inputs)
	decoder_lstm = tf.keras.layers.LSTM(GLOVE_DIM*2, return_state=True, return_sequences=True)
	decoder_outputs , _ , _ = decoder_lstm (decoder_embedding, initial_state=encoder_states)
	decoder_dense = tf.keras.layers.Dense(NUM_WORDS + 1, activation=tf.keras.activations.softmax) 
	output = decoder_dense(decoder_outputs)

	model = tf.keras.models.Model([encoder_inputs, decoder_inputs], output)
	model.compile(optimizer='adam', loss='categorical_crossentropy')
	return model, (decoder_lstm, decoder_dense, decoder_embedding, encoder_inputs, encoder_states, decoder_inputs, decoder_outputs)


def make_inference_models(decoder_lstm, decoder_dense, decoder_embedding, encoder_inputs, encoder_states, decoder_inputs, decoder_outputs):
    encoder_model = tf.keras.models.Model(encoder_inputs, encoder_states)    
    decoder_state_input_h = tf.keras.layers.Input(shape=(GLOVE_DIM*2,))
    decoder_state_input_c = tf.keras.layers.Input(shape=(GLOVE_DIM*2,))
    decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
    decoder_outputs, state_h, state_c = decoder_lstm(
        decoder_embedding , initial_state=decoder_states_inputs)
    decoder_states = [state_h, state_c]
    decoder_outputs = decoder_dense(decoder_outputs)
    decoder_model = tf.keras.models.Model(
        [decoder_inputs] + decoder_states_inputs,
        [decoder_outputs] + decoder_states)
    
    return encoder_model, decoder_model

def str_to_tokens(tokenizer, maxlen_questions, sentence):
	processed_sentence = text_preprocessing(pd.Series(sentence), CONTRACTIONS, LIST_PUNCT)[0]
	tokens_list = tokenizer.texts_to_sequences([processed_sentence])
	return preprocessing.sequence.pad_sequences(tokens_list, maxlen=maxlen_questions, padding='post')

def talk_to_chatbot(question, tokenizer, enc_model, dec_model, maxlen_questions, maxlen_answers, index2word_vocab):
	states_values = enc_model.predict(str_to_tokens(tokenizer, maxlen_questions, question))
	empty_target_seq = np.zeros((1, 1))
	empty_target_seq[0, 0] = tokenizer.word_index['startsentence']
	stop_condition = False
	decoded_translation = ''
	while not stop_condition:
		dec_outputs, h, c = dec_model.predict([empty_target_seq] + states_values )
		coin = np.random.choice([0, 1], p = (0.3, 0.7))
		if coin == 1:
			sampled_word_index = np.argmax(dec_outputs[0, -1, :])
		else:
			sampled_word_index = np.random.choice(range(len(dec_outputs[0, -1, :])), p = dec_outputs[0, -1, :])
		sampled_word = index2word_vocab[sampled_word_index]
		if sampled_word == 'endsentence' or len(decoded_translation.split()) > maxlen_answers:
			stop_condition = True
		else:
			decoded_translation += f' {sampled_word}'
		# Checks if answer reached its end
		empty_target_seq = np.zeros((1, 1))  
		empty_target_seq[0, 0] = sampled_word_index
		states_values = [h, c]
		decoded_translation = decoded_translation.replace(' threedots', r'\.\.\.')
	return decoded_translation

def main():
	(tokenizer, maxlen_questions, maxlen_answers, index2word_vocab) = pickle.load(open("variables.p", "rb"))
	model, model_layers = create_model()
	model.load_weights(CHECKPOINT_PATH)
	enc_model, dec_model = make_inference_models(*model_layers)
	response = talk_to_chatbot('Hi there!', tokenizer, enc_model, dec_model, maxlen_questions, maxlen_answers, index2word_vocab)
	print(response)

if __name__ == '__main__':
	main()
