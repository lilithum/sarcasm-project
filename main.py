"""Sarcastic Chatbot
Authors: Anna Faynveitz, Yulia Chudova, Yuri Kaz Kestenberg"""

#import files
from flask import Flask, render_template, request
from config import LONG_INPUT_ANS, EMPTY_INPUT_ANS, CONTRACTIONS, LIST_PUNCT
import numpy as np

from config import *
from utils import *
# import sarcasm_classifier as classifier
import tensorflow as tf
import pickle
from tensorflow.keras import layers , activations , models , preprocessing
from keras.layers import Embedding, Dense, LSTM
from keras.models import Model
import pandas as pd
from keras import utils
import re
import os
import pickle
import sarcasm_classifier_scorer
import time


app = Flask(__name__)


@app.route("/")
def home():
    return render_template('home.html')


def conv_contractions(question, contractions):
    "Convert all the contractions to the full form"
    for key, value in contractions.items():
        question = question.replace(key, value)
    return question

def weird_symbols(question):
    "Cleaning weird symbols"
    question = question.replace(r'http\S*', '').replace(
        r'[^A-Za-z*.,!?:;\s\d()\'\"]', '').replace(r'\.\.\.', ' threedots ')
    return question

def sep_punct(question, list_punct):
    "Add spaces before every punctuation"
    for p in list_punct:
        question = question.replace(p,' '+p)
    return question

def text_preprocessing(question, contractions, list_punct):
    question = question.lower()
    question = conv_contractions(question, contractions)
    question = weird_symbols(question)
    question = sep_punct(question, list_punct)
    return question

def readGloveFile(gloveFile):
    with open(gloveFile, 'r', encoding='utf8') as f:
        wordToGlove = {}  # map from a token (word) to a Glove embedding vector
        wordToIndex = {}  # map from a token to an index
        indexToWord = {}  # map from an index to a token

        for line in f:
            record = line.strip().split()
            token = record[0]  # take the token (word) from the text line
            wordToGlove[token] = np.array(record[1:],
                                          dtype=np.float64)  # associate the Glove embedding vector to a that token (word)

        tokens = sorted(wordToGlove.keys())
        for idx, tok in enumerate(tokens):
            kerasIdx = idx + 1  # 0 is reserved for masking in Keras (see above)
            wordToIndex[tok] = kerasIdx  # associate an index to a token (word)
            indexToWord[kerasIdx] = tok  # associate a word to a token (word). Note: inverse of dictionary above

    return wordToIndex, indexToWord, wordToGlove


# Create Pretrained Keras Embedding Layer
def createPretrainedEmbeddingLayer(wordToGlove, wordToIndex, isTrainable):
    vocabLen = len(wordToIndex) + 1  # adding 1 to account for masking
    print(vocabLen, 'vocablen')
    embDim = next(iter(wordToGlove.values())).shape[0]  # works with any glove dimensions (e.g. 50)

    embeddingMatrix = np.zeros((vocabLen, embDim))  # initialize with zeros
    for word, index in wordToIndex.items():
        embeddingMatrix[index, :] = wordToGlove[word]  # create embedding: word index to Glove word embedding

    embeddingLayer = tf.keras.layers.Embedding(vocabLen, embDim, weights=[embeddingMatrix], trainable=isTrainable)
    return embeddingLayer


def create_model():
    # prepare embeddings
    wordToIndex, indexToWord, wordToGlove = readGloveFile(r"glove.6B." + str(GLOVE_DIM) + "d.txt")
    pretrainedEmbeddingLayer = createPretrainedEmbeddingLayer(wordToGlove, wordToIndex, isTrainable=False)

    # Encoder part
    encoder_inputs = tf.keras.layers.Input(
        shape=(None,))  # Model() needs us to specify where inputs will enter the graph
    encoder_embedding = pretrainedEmbeddingLayer(encoder_inputs)
    encoder_outputs = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(GLOVE_DIM, return_state=True))
    encoder_outputs_, forward_h, forward_c, backward_h, backward_c = encoder_outputs(encoder_embedding)
    state_h = tf.keras.layers.Concatenate()([forward_h, backward_h])
    state_c = tf.keras.layers.Concatenate()([forward_c, backward_c])
    encoder_states = [state_h, state_c]

    # Decoder part
    decoder_inputs = tf.keras.layers.Input(shape=(None,))
    decoder_embedding = pretrainedEmbeddingLayer(decoder_inputs)
    decoder_lstm = tf.keras.layers.LSTM(GLOVE_DIM * 2, return_state=True, return_sequences=True)
    decoder_outputs, _, _ = decoder_lstm(decoder_embedding, initial_state=encoder_states)
    decoder_dense = tf.keras.layers.Dense(NUM_WORDS + 1, activation=tf.keras.activations.softmax)
    output = decoder_dense(decoder_outputs)

    model = tf.keras.models.Model([encoder_inputs, decoder_inputs], output)
    model.compile(optimizer='adam', loss='categorical_crossentropy')
    return model, (
    decoder_lstm, decoder_dense, decoder_embedding, encoder_inputs, encoder_states, decoder_inputs, decoder_outputs)


def make_inference_models(decoder_lstm, decoder_dense, decoder_embedding, encoder_inputs, encoder_states,
                          decoder_inputs, decoder_outputs):
    encoder_model = tf.keras.models.Model(encoder_inputs, encoder_states)
    decoder_state_input_h = tf.keras.layers.Input(shape=(GLOVE_DIM * 2,))
    decoder_state_input_c = tf.keras.layers.Input(shape=(GLOVE_DIM * 2,))
    decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
    decoder_outputs, state_h, state_c = decoder_lstm(
        decoder_embedding, initial_state=decoder_states_inputs)
    decoder_states = [state_h, state_c]
    decoder_outputs = decoder_dense(decoder_outputs)
    decoder_model = tf.keras.models.Model(
        [decoder_inputs] + decoder_states_inputs,
        [decoder_outputs] + decoder_states)

    return encoder_model, decoder_model


def str_to_tokens(tokenizer, maxlen_questions, sentence):
    tokens_list = tokenizer.texts_to_sequences([sentence])
    return preprocessing.sequence.pad_sequences(tokens_list, maxlen=maxlen_questions, padding='post')

def get_sarc_score(question, answer):
    question = text_preprocessing(question, CONTRACTIONS, LIST_PUNCT)
    answer = text_preprocessing(answer, CONTRACTIONS, LIST_PUNCT)
    return sarcasm_classifier_scorer.get_single_score(score_model, question, answer)

def talk_to_chatbot(question, tokenizer, enc_model, dec_model, maxlen_questions, maxlen_answers, index2word_vocab):
    question = text_preprocessing(question, CONTRACTIONS, LIST_PUNCT)
    states_values = enc_model.predict(str_to_tokens(tokenizer, maxlen_questions, question))
    empty_target_seq = np.zeros((1, 1))
    empty_target_seq[0, 0] = tokenizer.word_index['startsentence']
    stop_condition = False
    decoded_translation = ''
    while not stop_condition:
        dec_outputs, h, c = dec_model.predict([empty_target_seq] + states_values)
        coin = np.random.choice([0, 1], p=(0.3, 0.7))
        if coin == 1:
            sampled_word_index = np.argmax(dec_outputs[0, -1, :])
        else:
            sampled_word_index = np.random.choice(range(len(dec_outputs[0, -1, :])), p=dec_outputs[0, -1, :])
        sampled_word = index2word_vocab[sampled_word_index]
        if sampled_word == 'endsentence' or len(decoded_translation.split()) > maxlen_answers:
            stop_condition = True
        else:
            decoded_translation += f' {sampled_word}'
        # Checks if answer reached its end
        empty_target_seq = np.zeros((1, 1))
        empty_target_seq[0, 0] = sampled_word_index
        states_values = [h, c]

        # transform back
    decoded_translation = decoded_translation.replace(r' threedots ', r'...')
    decoded_translation =  re.sub(r'\s+([?,.!"])', r'\1', decoded_translation)
    decoded_translation = decoded_translation[0:2].upper() + decoded_translation[2:]
    decoded_translation = decoded_translation # + ' || Score:' +str(score)
    return decoded_translation

session = tf.Session()
tf.keras.backend.set_session(session)


(tokenizer, maxlen_questions, maxlen_answers, index2word_vocab) = pickle.load(open("variables.p", "rb"))
model, model_layers = create_model()
model.load_weights(CHECKPOINT_PATH)
# model._make_predict_function()
enc_model, dec_model = make_inference_models(*model_layers)
# enc_model._make_predict_function()
# dec_model._make_predict_function()
score_model = sarcasm_classifier_scorer.load_the_model()
score_model._make_predict_function()

@app.route("/get")
def get_bot_response():
    # Get user input
    userText = request.args.get('msg')

    # Processing user input
    if len(userText) >= 200:
        bot_response = np.random.choice(LONG_INPUT_ANS)
    elif len(userText) == 0:
        bot_response = np.random.choice(EMPTY_INPUT_ANS)
    else:
        bot_response = text_preprocessing(userText, CONTRACTIONS, LIST_PUNCT)
        score = 0
        while (score < 0.6):# or (score > 0.99):
            with session.as_default():
                with session.graph.as_default():
                    bot_response = talk_to_chatbot(userText, tokenizer, enc_model, dec_model, maxlen_questions, maxlen_answers, index2word_vocab)
            score = get_sarc_score(userText, bot_response)
        # Returning bot response
        bot_response = str(bot_response)   + ' || ' + str(f'{score:.2f}')
    return bot_response

if __name__ == "__main__":
    app.run(debug=True)



