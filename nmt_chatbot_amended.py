from config import *
from utils import *
# import sarcasm_classifier as classifier
import numpy as np
import tensorflow as tf
import pickle
from tensorflow.keras import layers , activations , models , preprocessing
from keras.layers import Embedding, Dense, LSTM
from keras.models import Model
import pandas as pd
from keras import utils
import re
import os
import pickle

def prepare_batch(batch_size, df_sarcastic, tokenizer, tokenized_questions, tokenized_answers, maxlen_questions, maxlen_answers):
    nmb_batches = len(df_sarcastic)//batch_size + 1
    while True:
        for i in range(nmb_batches):
            parent_comment = df_sarcastic['cleaned_parent_comment'][i*batch_size:(i+1)*batch_size]
            comment = df_sarcastic['cleaned_comment'][i*batch_size:(i+1)*batch_size]
            # encoder input data
            tokenized_questions = tokenizer.texts_to_sequences(parent_comment)
            padded_questions = preprocessing.sequence.pad_sequences(tokenized_questions, maxlen=maxlen_questions, padding='post' )
            encoder_input_data = np.array(padded_questions)

            # decoder_input_data
            tokenized_answers = tokenizer.texts_to_sequences(comment)
            padded_answers = preprocessing.sequence.pad_sequences(tokenized_answers, maxlen=maxlen_answers, padding='post' )
            decoder_input_data = np.array(padded_answers)

            # decoder_output_data
            tokenized_answers = tokenizer.texts_to_sequences(comment)
            for i in range(len(tokenized_answers)):
                tokenized_answers[i] = tokenized_answers[i][1:]
            padded_answers = preprocessing.sequence.pad_sequences(tokenized_answers, maxlen=maxlen_answers, padding='post' )
            onehot_answers = utils.to_categorical(padded_answers, NUM_WORDS + 1)
            decoder_output_data = np.array(onehot_answers)

            yield [encoder_input_data, decoder_input_data], decoder_output_data


def readGloveFile(gloveFile):
    with open(gloveFile, 'r', encoding='utf8') as f:
        wordToGlove = {}  # map from a token (word) to a Glove embedding vector
        wordToIndex = {}  # map from a token to an index
        indexToWord = {}  # map from an index to a token 

        for line in f:
            record = line.strip().split()
            token = record[0] # take the token (word) from the text line
            wordToGlove[token] = np.array(record[1:], dtype=np.float64) # associate the Glove embedding vector to a that token (word)

        tokens = sorted(wordToGlove.keys())
        for idx, tok in enumerate(tokens):
            kerasIdx = idx + 1  # 0 is reserved for masking in Keras (see above)
            wordToIndex[tok] = kerasIdx # associate an index to a token (word)
            indexToWord[kerasIdx] = tok # associate a word to a token (word). Note: inverse of dictionary above

    return wordToIndex, indexToWord, wordToGlove

# Create Pretrained Keras Embedding Layer
def createPretrainedEmbeddingLayer(wordToGlove, wordToIndex, isTrainable):
    vocabLen = len(wordToIndex) + 1  # adding 1 to account for masking
    print(vocabLen, 'vocablen')
    embDim = next(iter(wordToGlove.values())).shape[0]  # works with any glove dimensions (e.g. 50)

    embeddingMatrix = np.zeros((vocabLen, embDim))  # initialize with zeros
    for word, index in wordToIndex.items():
        embeddingMatrix[index, :] = wordToGlove[word] # create embedding: word index to Glove word embedding

    embeddingLayer = tf.keras.layers.Embedding(vocabLen, embDim, weights=[embeddingMatrix], trainable=isTrainable)
    return embeddingLayer

def prepare_data(path = 'train-balanced-sarcasm.csv', path_score = 'sarcasm_score.csv'):
	df = pd.read_csv(path)
	df_sarc_score = pd.read_csv(path_score, index_col  = 0, header = None, names = ['sarc_score'])
	df.drop('author', axis=1, inplace=True)
	df = pd.concat([df, df_sarc_score], axis = 1)
	df.dropna(how='any', inplace=True)
	df['sarc_score'] = df['sarc_score'].apply(float)
	df = df[df['sarc_score']>THRESHOLD]
	df_sarcastic = df[['comment', 'parent_comment', 'score']][df['label']==1]
	df_sarcastic = df_sarcastic[df_sarcastic['comment'].str.len() < 200]
	df_sarcastic = df_sarcastic[df_sarcastic['parent_comment'].str.len() < 200]
	df_sarcastic = df_sarcastic[df_sarcastic['score'] >= 0]
	df_sarcastic['comment'] = 'startsentence '+ df_sarcastic['comment'] + ' endsentence'
	df_sarcastic['cleaned_comment'] = text_preprocessing(df_sarcastic['comment'], CONTRACTIONS, LIST_PUNCT)
	df_sarcastic['cleaned_parent_comment'] = text_preprocessing(df_sarcastic['parent_comment'], CONTRACTIONS, LIST_PUNCT)
	df_sarcastic.drop(['comment', 'parent_comment'], axis=1, inplace=True)
	df_sarcastic = df_sarcastic[['cleaned_comment','cleaned_parent_comment']]
	print("Lines in dataset: ", len(df_sarcastic))
	return df_sarcastic

# Tokenizer
def prepare_tokenizer(df_sarcastic):
	tokenizer = preprocessing.text.Tokenizer(oov_token=OOV_TOKEN, filters='"#$%&()*+/;<=>@[\\]^_`{|}~\t\n')
	tokenizer.fit_on_texts(df_sarcastic['cleaned_comment'].apply(str).tolist() + df_sarcastic['cleaned_parent_comment'].apply(str).tolist())
	tokenizer.word_index = {e:i for e,i in tokenizer.word_index.items() if i <= NUM_WORDS} # <= because tokenizer is 1 indexed
	VOCAB_SIZE = len(tokenizer.word_index) + 1
	print(f'VOCAB SIZE: {VOCAB_SIZE}')
	tokenized_questions = tokenizer.texts_to_sequences(df_sarcastic['cleaned_parent_comment'].apply(str))
	tokenized_answers = tokenizer.texts_to_sequences(df_sarcastic['cleaned_comment'].apply(str))

	maxlen_questions = max([len(x) for x in tokenized_questions])
	maxlen_answers = max([len(x) for x in tokenized_answers])
	return (tokenizer, tokenized_questions, tokenized_answers, maxlen_questions, maxlen_answers)

def create_model():
	#prepare embeddings
	wordToIndex, indexToWord, wordToGlove = readGloveFile(r"glove.6B." + str(GLOVE_DIM)+ "d.txt")
	pretrainedEmbeddingLayer = createPretrainedEmbeddingLayer(wordToGlove, wordToIndex, isTrainable=False)

	#Encoder part
	encoder_inputs = tf.keras.layers.Input(shape=(None,))   # Model() needs us to specify where inputs will enter the graph
	encoder_embedding = pretrainedEmbeddingLayer(encoder_inputs)
	encoder_outputs =  tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(GLOVE_DIM, return_state=True))
	encoder_outputs_, forward_h, forward_c, backward_h, backward_c = encoder_outputs(encoder_embedding)
	state_h = tf.keras.layers.Concatenate()([forward_h, backward_h])
	state_c = tf.keras.layers.Concatenate()([forward_c, backward_c])
	encoder_states = [state_h, state_c]

	#Decoder part
	decoder_inputs = tf.keras.layers.Input(shape=(None,))
	decoder_embedding = pretrainedEmbeddingLayer(decoder_inputs)
	decoder_lstm = tf.keras.layers.LSTM(GLOVE_DIM*2, return_state=True, return_sequences=True)
	decoder_outputs , _ , _ = decoder_lstm (decoder_embedding, initial_state=encoder_states)
	decoder_dense = tf.keras.layers.Dense(NUM_WORDS + 1, activation=tf.keras.activations.softmax) 
	output = decoder_dense(decoder_outputs)

	model = tf.keras.models.Model([encoder_inputs, decoder_inputs], output)
	model.compile(optimizer='adam', loss='categorical_crossentropy')
	return model

def train_model(model, df_sarcastic, load_weights, tokenizer_data):
	batch_generator = prepare_batch(BATCH_SIZE, df_sarcastic, *tokenizer_data)
	checkpoint_dir = os.path.dirname(CHECKPOINT_PATH)
	cp_callback = tf.keras.callbacks.ModelCheckpoint(CHECKPOINT_PATH,
	                                                 save_weights_only=True,
	                                                 verbose=1, load_weights_on_restart = True,
	                                                 save_freq='epoch')
	if LOAD_WEIGHTS:
		model.load_weights(CHECKPOINT_PATH)

	model.fit_generator(batch_generator, steps_per_epoch=len(df_sarcastic)//BATCH_SIZE, epochs=EPOCHS,
	                    use_multiprocessing = USE_MULTIPROCESSING, workers = WORKERS, callbacks = [cp_callback])
	model.save('model.h5') 

def main():
	df_sarcastic = prepare_data(DATA_PATH)
	# tokenizer_data = prepare_tokenizer(df_sarcastic)
	(tokenizer, maxlen_questions, maxlen_answers, index2word_vocab) = pickle.load(open("variables.p", "rb"))
	tokenized_questions = tokenizer.texts_to_sequences(df_sarcastic['cleaned_parent_comment'].apply(str))
	tokenized_answers = tokenizer.texts_to_sequences(df_sarcastic['cleaned_comment'].apply(str))
	model = create_model()
	tokenizer_data = (tokenizer, tokenized_questions, tokenized_answers, maxlen_questions, maxlen_answers)
	train_model(model, df_sarcastic, LOAD_WEIGHTS, tokenizer_data)


if __name__ == '__main__':
	main()
