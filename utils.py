def conv_contractions(series, contractions):
    "Convert all the contractions to the full form"
    for key, value in contractions.items():
        series = series.str.replace(key, value)
    return series

def weird_symbols(series):
    "Cleaning weird symbols"
    series = series.str.replace(r'http\S*', '').str.replace(
        r'[^A-Za-z*.,!?:;\s\d()\'\"]', '').str.replace(r'\.\.\.', ' threedots ')
    return series

def sep_punct(series, list_punct):
    "Add spaces before every punctuation"
    for p in list_punct:
        series = series.str.replace(p,' '+p)
    return series

def text_postprocessing(series, list_punct):
    "Remove spaces before punctuation"
    for p in list_punct:
        series = series.str.replace(' '+p, p)
    return series

def text_preprocessing(series, contractions, list_punct):
    series = series.apply(str).str.lower()
    series = conv_contractions(series, contractions)
    series = weird_symbols(series)
    series = sep_punct(series, list_punct)
    return series
